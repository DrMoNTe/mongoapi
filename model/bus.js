const mongoose = require('mongoose');
const wpModule = require('../func/waypoint_system');
const busSchema = mongoose.Schema({
	device_name:{
		type: String,
		required: true
	},
	/////////////////
	bus:{
		type: String,

	},
	bus_path:{
		type: String,
	},

	////////////////
	default_path:{
		type:String,
		require:true
	},

	near_wp:{
		type: Number,
	},
	wp_lat:{
			type: Number
	},
	wp_lng:{
			type: Number,
	},
	lat:{
        type: Number,
        required: true
	},
	lng:{
		type: Number,
		required: true
	},
	ave_velocity:{
		type: Number,
		
	},
	velocity:{
		type: Number,
		required: true
	},
	time:{
		type: Date,
		default: Date.now
	}
});

const Bus = module.exports = mongoose.model('bus', busSchema,'bus');

// Get Bus Location
module.exports.getBus = (callback, limit) => {
	Bus.find(callback).limit(limit);
}

// Get a Bus Location
module.exports.getBusById = (id, callback) => {
	Bus.findById(id, callback);
}

// Add Bus
module.exports.addBus = (bus, callback) => {
	var lat = bus.lat;
	var lng = bus.lng;
	near = wpModule.findNearest({lat,lng});
	bus.near_wp = near.index;
	bus.wp_lat = near.lat;
	bus.wp_lng = near.lng;
	Bus.create(bus, callback);
}


// Update Bus /////////////// NOT AVAILABLE FOR UPDATE
module.exports.updateBus = (id, bus, options, callback) => {
	var query = {_id: id};
	var update = {
		device_name: bus.device_name,
		bus: bus.bus,
		bus_path:bus.bus_path,
        default_path: bus.default_path,
        lat:bus.lat,
		lng:bus.lng,
		ave_velocity: bus.ave_velocity,
		velocity: bus.velocity,
        time:bus.time
	}
	var lat = bus.lat;
	var lng = bus.lng;
	near = wpModule.findNearest({lat,lng});
	update.near_wp = near.index;
	update.wp_lat = near.lat;
	update.wp_lng = near.lng;
	Bus.findByIdAndUpdate(query, update, options, callback);
}

function convertUTCDateToLocalDate(date) {
    var newDate = new Date(date.getTime()+date.getTimezoneOffset()*60*1000);

    var offset = date.getTimezoneOffset() / 60;
    var hours = date.getHours();

    newDate.setHours(hours - offset);

    return newDate;   
}

// Delete Bus
module.exports.removeBus = (id, callback) => {
	var query = {_id: id};
	Bus.remove(query, callback);
}
