const mongoose = require('mongoose');
//const wpModule = require('../func/waypoint_system');
const bus_infoSchema = mongoose.Schema({
    device_name:{
		type: String,
		required: true
    },
	name:{
		type: String,
		required: true
    },
    path:{
        type: String,
		required: true
    },
	decs:{
		type: String
	},


});

const Bus_info = module.exports = mongoose.model('bus_info', bus_infoSchema,'bus_info');

module.exports.getBus_info = (callback, limit) => {
	Bus_info.find(callback).limit(limit);
};

module.exports.getBus_infoById = (id, callback) => {
	Bus_info.findById(id, callback);
};

// Add Bus
module.exports.addBus_info = (bus_info, callback) => {
	Bus_info.create(bus_info, callback);
};

// Update Bus
module.exports.updateBus_info = (id, bus_info, options, callback) => {
	var query = {_id: id};
	var update = {
        device_name: bus_info.device_name,
        name: bus_info.name,
        path: bus_info.path,
        decs:bus_info.decs

    }
	Bus_info.findByIdAndUpdate(query, update, options, callback);
};

// Delete Bus
module.exports.removeBus_info = (id, callback) => {
	var query = {_id: id};
	Bus_info.remove(query, callback);
};