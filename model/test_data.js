const mongoose = require('mongoose');

const Schema = mongoose.Schema({
	data:{
		type: String,
		required: true
	},

});
var LoginSchema = new mongoose.Schema({
    login:{
        password: String,
        username: String,
        Permission: Number
    }
});

const Data = module.exports = mongoose.model('test_data', Schema,'data_test');


module.exports.getData = (callback, limit) => {
	Data.find(callback).limit(limit);
}

module.exports.addData = (data, callback) => {
	Data.create(data, callback);
}

module.exports.removeData = (id, callback) => {
	var query = {_id: id};
	Data.remove(query, callback);
}