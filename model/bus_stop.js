const mongoose = require('mongoose');
const wpModule = require('../func/waypoint_system');
const bus_stopSchema = mongoose.Schema({
	name:{
		type: String,
		required: true
	},
	path:{
        type: String,
        required: true
	},
	decs:{
		type: String
	},
	near_wp:{
		type: Number,
	},
	wp_lat:{
			type: Number
	},
	wp_lng:{
			type: Number,
	},
	lat:{
        type: Number,
        required: true
	},
	lng:{
		type: Number,
		required: true
	}

});

const Bus_stop = module.exports = mongoose.model('bus_stop', bus_stopSchema,'bus_stop');

module.exports.getBus_stop = (callback, limit) => {
	Bus_stop.find(callback).limit(limit);
}

module.exports.getBus_stopById = (id, callback) => {
	Bus_stop.findById(id, callback);
}

// Add Bus
module.exports.addBus_stop = (bus_stop, callback) => {
	var lat = bus_stop.lat;
	var lng = bus_stop.lng;
	near = wpModule.findNearest({lat,lng});
	bus_stop.near_wp = near.index;
	bus_stop.wp_lat = near.lat;
	bus_stop.wp_lng = near.lng;
	Bus_stop.create(bus_stop, callback);
}

// Update Bus
module.exports.updateBus_stop = (id, bus_stop, options, callback) => {
	var query = {_id: id};
	var update = {
		name: bus_stop.name,
		path: bus_stop.path,
        decs: bus_stop.decs,
        lat:bus_stop.lat,
        lng:bus_stop.lng,

	}
	var lat = bus_stop.lat;
	var lng = bus_stop.lng;
	near = wpModule.findNearest({lat,lng});
	update.near_wp = near.index;
	update.wp_lat = near.lat;
	update.wp_lng = near.lng;
	Bus_stop.findByIdAndUpdate(query, update, options, callback);
}

// Delete Bus
module.exports.removeBus_stop = (id, callback) => {
	var query = {_id: id};
	Bus_stop.remove(query, callback);
}