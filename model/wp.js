const mongoose = require('mongoose');
const wpModule = require('../func/waypoint_system');
const wpSchema = mongoose.Schema({
	name:{
		type: String,
		required: true
	},
	index:{
		type: Number,
		required:true
	},
	lat:{
        type: Number,
        required: true
	},
	lng:{
		type: Number,
		required: true
	}

});

const Wp = module.exports = mongoose.model('waypoint', wpSchema,'waypoint');

module.exports.getWaypoint = (callback, limit) => {
	Wp.find(callback).limit(limit);
}

module.exports.getWaypointByID = (id, callback) => {
	Wp.findById(id, callback);
}

// Add Bus
/* module.exports.addBus_stop = (bus_stop, callback) => {
	var lat = bus_stop.lat;
	var lng = bus_stop.lng;
	near = wpModule.findNearest({lat,lng});
	bus_stop.near_wp = near.name;
	Bus_stop.create(bus_stop, callback);
}

// Update Bus
module.exports.updateBus_stop = (id, bus_stop, options, callback) => {
	var query = {_id: id};
	var update = {
        name: bus_stop.name,
        near_wp: bus_stop.near_wp,
        lat:bus_stop.lat,
        lng:bus_stop.lng,

    }
	Bus_stop.findOneAndUpdate(query, update, options, callback);
}

// Delete Bus
module.exports.removeBus_stop = (id, callback) => {
	var query = {_id: id};
	Bus_stop.remove(query, callback);
} */