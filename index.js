var express = require("express");
var app = express();
var http = require("http").Server(app);
var bodyParser = require("body-parser");
var mongoose = require("mongoose");
var io = require("socket.io")(http);
var cors = require("cors");
var sma = require("sma");
app.use(bodyParser.json());
//app.use(cors);

Bus = require("./model/bus");
Bus_stop = require("./model/bus_stop");
Bus_info = require("./model/bus_info");
Wp = require("./model/wp");
Testdata = require("./model/test_data");
const wpModule = require("./func/waypoint_system");

var smaList = {};
mongoose.connect(
	"mongodb+srv://admin:admin@cluster0-qv3ln.gcp.mongodb.net/bus_tracking"
);
var db = mongoose.connection;
app.options("*", cors());
app.use(function (req, res, next) {
	res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
	res.header(
		"Access-Control-Allow-Headers",
		"Origin, X-Requested-With, Content-Type, Accept"
	);
	next();
});

app.get("/", function (req, res) {
	res.sendfile("index.html");
});

////////////////////  BUS  ///////////////////////////

app.get("/api/bus", function (req, res, next) {
	Bus.getBus(function (err, data) {
		if (err) {
			res.status(400).json(err);
		}
		var d = new Date();
		var package = {
			length: data.length,
			last_time: d.toJSON(),
			data: data,
		};
		res.status(200).json(package);
	});
});

app.get("/api/bus/:_id", (req, res, next) => {
	Bus.getBusById(req.params._id, (err, data) => {
		if (err) {
			res.status(400).json(err);
		}
		res.status(200).json(data);
	});
});

app.post("/api/bus", function (req, res, next) {
	Bus_info.getBus_info(function (err, data) {
		var body = req.body;
		var temp = body.device_name;
		var velo_km = body.velocity;
		var velo = (velo_km / 3.6).toFixed(5);
		body.velocity = velo;
		console.log("VELO_M", velo);
		if (err) {
			res.status(400).json(err);
		}

		for (var i = 0; i <= Object.keys(data).length - 1; i++) {
			console.log("DATA", data[i]);
			if (body.device_name == data[i].device_name) {
				console.log("MATCH THE BUS");
				body.bus = data[i].name;
				body.bus_path = data[i].path;
				break;
			}
		}

		if (smaList[temp] == null) {
			new_list = [];
			//smaList[temp] = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,velo];
			for (let i = 0; i < 20; i++) {
				new_list.push(0);
			}
			new_list.push(velo);
			smaList[temp] = new_list;
		} else {
			smaList[temp].push(velo);
			smaList[temp].shift();
		}

		console.log("BODY", body);
		temp_list = sma(smaList[temp], 20);
		body.ave_velocity = temp_list[temp_list.length - 1];
		console.log("SMALIST", smaList);
		console.log("LASTSMA", temp_list[temp_list.length - 1]);
		Bus.addBus(body, function (err, databus) {
			if (err) {
				res.status(400).json(err);
			}

			io.sockets.emit("sendData", databus);
			res.status(200).json(databus);
		});
	});
});

// THIS BUS NOT AVAILABLE FOR EDIT/UPDATE
app.put("/api/bus/:_id", (req, res) => {
	var id = req.params._id;
	var body = req.body;
	Bus.updateBus(id, body, { new: true }, (err, data) => {
		if (err) {
			res.status(400).json(err);
		}
		res.status(200).json(data);
	});
});

app.delete("/api/bus/:_id", (req, res) => {
	var id = req.params._id;
	Bus.removeBus(id, (err, data) => {
		if (err) {
			res.status(400).json(err);
		}
		res.status(200).json("Delete Success");
	});
});

////////////////////////////////////////////////////////

////////////////////  BUS_INFO  ///////////////////////////
app.get("/api/bus_info", function (req, res, next) {
	Bus_info.getBus_info(function (err, data) {
		if (err) {
			res.status(400).json(err);
		}
		var d = new Date();
		var package = {
			length: data.length,
			last_time: d.toJSON(),
			data: data,
		};
		res.status(200).json(package);
	});
});

app.get("/api/bus_info/:_id", (req, res, next) => {
	var id = req.params._id;
	Bus_info.getBus_infoById(id, (err, data) => {
		if (err) {
			res.status(400).json(err);
		}
		res.status(200).json(data);
	});
});

app.post("/api/bus_info", (req, res, next) => {
	var body = req.body;
	Bus_info.addBus_info(body, (err, data) => {
		if (err) {
			res.status(400).json(err);
		}
		res.status(200).json(data);
	});
});

app.put("/api/bus_info/:_id", (req, res, next) => {
	var id = req.params._id;
	var body = req.body;
	Bus_info.updateBus_info(id, body, { new: true }, (err, data) => {
		if (err) {
			res.status(400).json(err);
		}
		res.status(200).json(data);
	});
});

app.delete("/api/bus_info/:_id", (req, res, next) => {
	var id = req.params._id;
	Bus_info.removeBus_info(id, (err, data) => {
		if (err) {
			res.status(400).json(err);
		}
		res.status(200).json("Delete Success");
	});
});

///////////////////////////////////////////////////////////////////

////////////////////////////  BUS_STOP  ////////////////////////////////////////

app.get("/api/bus_stop", function (req, res, next) {
	Bus_stop.getBus_stop(function (err, data) {
		if (err) {
			res.status(400).json(err);
			//throw err;
		}
		var d = new Date();
		var package = {
			length: data.length,
			last_time: d.toJSON(),
			data: data,
		};
		res.status(200).json(package);
	});
});

app.get("/api/bus_stop/:_id", (req, res, next) => {
	var id = req.params._id;
	Bus_stop.getBus_stopById(id, (err, data) => {
		if (err) {
			res.status(400).json(err);
			//throw err;
		}
		res.status(200).json(data);
	});
});

app.post("/api/bus_stop", (req, res, next) => {
	var body = req.body;
	Bus_stop.addBus_stop(body, (err, data) => {
		if (err) {
			res.status(400).json(err);
		}
		res.status(200).json(data);
	});
});

app.put("/api/bus_stop/:_id", (req, res, next) => {
	var id = req.params._id;
	var body = req.body;
	Bus_stop.updateBus_stop(id, body, { new: true }, (err, data) => {
		if (err) {
			res.status(400).json(err);
		}
		res.status(200).json(data);
	});
});

app.delete("/api/bus_stop/:_id", (req, res, next) => {
	var id = req.params._id;
	Bus_stop.removeBus_stop(id, (err, data) => {
		if (err) {
			res.status(400).json(err);
		}
		res.status(200).json("Delete Success");
	});
});

//////////////////////////////////////////////////////////

///////////////////// ADDITIONAL  ///////////////////////////

app.post("/api/func/findNearestBus_stop", function (req, res, next) {
	Bus_stop.getBus_stop(function (err, data) {
		if (err) {
			res.status(400).json(err);
		}
		console.log(data);
		var val = wpModule.findNearestBus_stop(req.body, data);
		res.status(200).json(val);
	});
});

app.delete("/api/func/SMAList/:name", function (req, res, next) {
	name = req.params.name;
	if (name == "clear") {
		smaList = {};
	} else {
		delete smaList[name];
	}

	//res.statusMessage("clear success");
	res.status(200).json(smaList);
});

app.get("/api/func/SMAList", function (req, res, next) {
	res.status(200).json(smaList);
});

app.get("/api/func/wp", function (req, res, next) {
	data = wpModule.ldWaypoint();
	res.status(200).json(data);
});
app.get("/api/path/red", function (req, res, next) {
	data = wpModule.redBus();
	res.status(200).json(data);
});
app.get("/api/path/yellow", function (req, res, next) {
	data = wpModule.yellowBus();
	res.status(200).json(data);
});

app.get("/api/func/waypoint", function (req, res, next) {
	Wp.getWaypoint(function (err, data) {
		if (err) {
			res.status(400).json(err);
		}
		res.status(200).json(data);
	});
});

app.post("/api/func/findNearest", function (req, res, next) {
	val = wpModule.findNearest(req.body);
	res.status(200).json(val);
});

app.post("/api/func/findDist", function (req, res, next) {
	val = wpModule.calWPdist(req.body);
	res.status(200).json(val);
});

app.post("/api/func/findTime", function (req, res, next) {
	val = wpModule.estTime(req.body);

	res.status(200).json(val);
});
app.get("/testdata", function (req, res, next) {
	Testdata.getData(function (err, data) {
		if (err) {
			res.status(400).json(err);
		}
		res.status(200).json(data);
	});
});
app.post("/testdata", (req, res, next) => {
	var body = req.body;
	Testdata.addData(body, (err, data) => {
		if (err) {
			res.status(400).json(err);
		}
		res.status(200).json(data);
	});
});

app.delete("/testdata/:_id", (req, res, next) => {
	var id = req.params._id;
	Testdata.removeData(id, (err, data) => {
		if (err) {
			res.status(400).json(err);
		}
		res.status(200).json(data);
	});
});

http.listen(3000, function () {
	console.log("start server on port :3000");
});
